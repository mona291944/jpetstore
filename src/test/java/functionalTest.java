import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class functionalTest {
	private static final String String = null;
	public String URL ="http://demo.kieker-monitoring.net/jpetstore/actions/Catalog.action";
	 String driverpath = "C:\\eclipse-workspace\\javaSelenium\\driver\\chromedriver.exe";
	 WebDriver driver;
	 WebElement UserID,PassWord,Name1,Name2,email,phone,add1,add2,city,state,zip,cntry;
	 		
 @BeforeTest
 public void launchbrowser() throws InterruptedException {
	  System.out.println("launching Chrome browser"); 
	  System.setProperty("webdriver.chrome.driver", driverpath);
	  driver = new ChromeDriver();
	  driver.get(URL);
	  driver.manage().window().maximize();
    }   
 @DataProvider 
 public Object[][] custinfo() throws IOException{
	 return new Object[][] 
		    	{
		          //  { "Mo1", "PW1", "Monalisa1", "Panda", "mona1@asd.com", "123-456-7895", "Main STREET", "LANE2", "LONE TREE", "CO", "80124","US", "japanese", "DOGS"},
		          //  {"Mo2","PW2","Monalisa2","Panda","mona2@asd.com","123-456-7895","Main STREET","LANE2","LONE TREE","CO","80124","US","english","FISH"},
		            { "Mo3", "PW3", "Monalisa3", "Panda", "mona3@asd.com", "123-456-7895", "Main STREET", "LANE2", "LONE TREE", "CO", "80124","US", "japanese", "DOGS"},
		        };

		    }
	
 /*private Object[][] read() throws IOException {
	 String file = "C:\\eclipse-workspace\\JPetStore\\Test Data.xlsx";
	 
	 File src = new File(file);
	 FileInputStream FIS = new FileInputStream(src);
	 XSSFWorkbook wb = new XSSFWorkbook(FIS);
	 XSSFSheet sh = wb.getSheetAt(0);
	 
	 
	 //XSSFRow row = null;
	 //row=sh.getRow(0);
	 int colcount = sh.getRow(0).getLastCellNum();  //row.getLastCellNum();
	 int rowcount = sh.getLastRowNum();
	 System.out.println("row-"+rowcount + "column-"+colcount);
			 
	Object[][] data = new Object[rowcount][colcount];
		
		for(int i=0;i<rowcount;i++) {
			XSSFRow row = sh.getRow(i);
			for(int j=0; j<colcount;j++) {
				XSSFCell cell = row.getCell(j);
				//data[i][j]= sh.getRow(i).getCell(j).getStringCellValue();
				DataFormatter formatter = new DataFormatter();
                String val = formatter.formatCellValue(cell);
                data[i - 1][j] = val;
			}
		}
	 
	return data;
	
}*/
@Test(dataProvider="custinfo", priority=1, enabled =false)
 public void SignUp(String UID, String PASS, String FNAME,String LNAME, String MAIL,String PH,String A1, String A2, String CTY,
		 String ST,String ZP,String CNTRY, String PREF,String CAT) {
	  
	 driver.findElement(By.linkText("Sign In")).click();	 
	 driver.findElement(By.linkText("Register Now!")).click();
	 UserID =driver.findElement(By.name("username"));
	 UserID.sendKeys(UID);
	 PassWord =driver.findElement(By.name("password"));
	 PassWord.sendKeys(PASS);
	 driver.findElement(By.name("repeatedPassword")).sendKeys(PASS);
	 Name1=driver.findElement(By.name("account.firstName"));
	 Name1.sendKeys(FNAME);
	 Name2=driver.findElement(By.name("account.lastName"));
	 Name2.sendKeys(LNAME);
	 email=driver.findElement(By.name("account.email"));
	 email.sendKeys(MAIL);
	 phone=driver.findElement(By.name("account.phone"));
	 phone.sendKeys(PH);
	 add1=driver.findElement(By.name("account.address1"));
	 add1.sendKeys(A1);
	 add2=driver.findElement(By.name("account.address2"));
	 add2.sendKeys(A2);
	 city=driver.findElement(By.name("account.city"));
	 city.sendKeys(CTY);
	 state=driver.findElement(By.name("account.state"));
	 state.sendKeys(ST);
	 zip=driver.findElement(By.name("account.zip"));
	 zip.sendKeys(ZP);
	 cntry=driver.findElement(By.name("account.country"));
	 cntry.sendKeys(CNTRY);
	 Select p = new Select(driver.findElement(By.name("account.languagePreference")));
	 p.selectByVisibleText(PREF);
	 Select C = new Select(driver.findElement(By.name("account.favouriteCategoryId")));
	 C.selectByVisibleText(CAT);
	 driver.findElement(By.name("newAccount")).click();
	 driver.findElement(By.linkText("Sign Out")).click();
	 driver.get(URL);
 
	 
 }
@Test(priority=2, enabled =false)
@Parameters({"UserID","Password"})
public void SignIn(String UID, String PWD) {
		Login(UID, PWD);
}
private void Login(String UID, String PWD) {
	try {
		driver.findElement(By.linkText("Sign In")).click();
		driver.findElement(By.name("username")).clear();
		driver.findElement(By.name("username")).sendKeys(UID);
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys(PWD);
		driver.findElement(By.name("signon")).click();
		if (driver.findElement(By.id("Welcome")) != null) {
			System.out.println("User " + UID + " Login is successful");
		} else {
			System.out.println("User " + UID + " Login is Failed");
		}
	} catch (Exception e) {
		System.out.println("UserLogin Failed, Exception:" + e);
	}
}
@DataProvider 
public Object[][] OrderItem() throws IOException{
	 return new Object[][] 
		    	{
		          
		            {"Fish","Angelfish", "FI-SW-01","EST-2"},
		            { "Dogs","Bulldog", "K9-BD-01","EST-7"},
		           // {"Cats","Manx","FL-DSH-01","EST-15"},
		           // {"Reptiles","Iguana","RP-LI-02","EST-13"},
		           // {"Birds", "Amazon Parrot","AV-CB-01","EST-18"}
		        };

		    }


@Test(dataProvider="OrderItem", priority=1, enabled =false)
//@Parameters({"UserID","Password"})

public void productBrowse(String Category, String Name, String ProdID, String ItemID) throws Exception {
	
	if (Category.equalsIgnoreCase("Fish")) {
		driver.findElement(By.xpath("//*[@id=\"SidebarContent\"]/a[1]/img")).click();	
		order(Name,ProdID,ItemID);
		
	}if(Category.equalsIgnoreCase("Dogs")){
		driver.findElement(By.xpath("//*[@id=\"SidebarContent\"]/a[2]/img")).click();
		order(Name,ProdID,ItemID);
	}if(Category.equalsIgnoreCase("Cats")){
		driver.findElement(By.xpath("//*[@id=\"SidebarContent\"]/a[3]/img")).click();
		order(Name,ProdID,ItemID);
	}if(Category.equalsIgnoreCase("Reptiles")){
		driver.findElement(By.xpath("//*[@id=\"SidebarContent\"]/a[4]/img")).click();
		order(Name,ProdID,ItemID);
	}if(Category.equalsIgnoreCase("Birds")){
		driver.findElement(By.xpath("//*[@id=\"SidebarContent\"]/a[5]/img")).click();
		order(Name,ProdID,ItemID);
	}
	driver.findElement(By.linkText("Proceed to Checkout")).click();
	
	
	
	    	
	    }

private void order(String name, String prodID, String ItemID) throws Exception {
	List<WebElement> Cols = driver.findElements(By.xpath("//*[@id=\"Catalog\"]/table/tbody/tr[1]/th"));
	System.out.println("Column size:"+Cols.size());
	List<WebElement> Rows = driver.findElements(By.xpath("//*[@id=\"Catalog\"]/table/tbody/tr"));
	System.out.println("Row size:"+Rows.size());
	driver.findElement(By.linkText(prodID)).click();
	driver.findElement(By.linkText(ItemID)).click();
	driver.findElement(By.linkText("Add to Cart")).click();
	driver.findElement(By.xpath("//*[@id=\"LogoContent\"]/a/img")).click();
	
    }
	
}


